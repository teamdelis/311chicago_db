#!/bin/bash

USER="postgres"

# add tables and functions to the empty database
sudo -u $USER psql chicago311_db < chicago311_db.sql
sudo -u $USER psql chicago311_db < stored_functions.sql

# parse csv files
sudo -u $USER psql chicago311_db -c '\copy request_type FROM clean_data/request_types.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy street_names FROM clean_data/street_names.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy zip_codes FROM clean_data/zip_codes.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy request FROM clean_data/request.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy ssa_in_request FROM clean_data/ssa_in_request.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy activities FROM clean_data/current_activities.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy activities_per_type FROM clean_data/current_activities_per_type.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy actions FROM clean_data/most_recent_actions.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy actions_per_type FROM clean_data/most_recent_actions_per_type.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy recent_actions_on_request FROM clean_data/actions_upon_request.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy garbage_carts_delivered FROM clean_data/garbage_carts_delivered.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy potholes_filled FROM clean_data/potholes_filled.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy location_of_tree_in_request FROM clean_data/debris_location.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy sanitation_code_violations FROM clean_data/sanitation_code_violations.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy sanitation_violations_in_request FROM clean_data/sanitation_code_violations_in_request.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy rodent_baiting_details FROM clean_data/rodent_details.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy surfaces_with_graffiti FROM clean_data/surfaces_with_graffiti.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy structures_with_graffiti FROM clean_data/structures_with_graffiti.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy graffiti_details FROM clean_data/graffiti_details.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy vehicle_make_model FROM clean_data/vehicle_make_models.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy vehicle_color FROM clean_data/vehicle_colors.csv CSV'
sudo -u $USER psql chicago311_db -c '\copy abandoned_vehicle_details FROM clean_data/vehicle_details.csv CSV'