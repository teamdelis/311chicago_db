-- CREATE DATABASE IF NOT EXISTS chicago311_db;

-- USE chicago311_db;

create schema public;
comment on schema public is 'standard public schema';

alter schema public owner to postgres;

CREATE TYPE request_status_enum AS ENUM ('Completed', 'Completed - Dup', 'Open', 'Open - Dup');
CREATE TYPE tree_locations_enum AS ENUM ('Alley', 'Vacant Lot', 'Parkway');

create sequence hibernate_sequence;
alter sequence hibernate_sequence owner to panagiotis;
create sequence request_id_sequence;
alter sequence request_id_sequence owner to panagiotis;


create table if not exists actions_per_type
(
	action varchar(255) not null,
	request_type varchar(255) not null,
	constraint actions_per_type_pkey
		primary key (request_type, request_type)
)
;

alter table actions_per_type owner to panagiotis;

create table if not exists activities_per_type
(
	activity varchar(255) not null,
	request_type varchar(255) not null,
	constraint activities_per_type_pkey
		primary key (request_type, request_type)
)
;

alter table activities_per_type owner to panagiotis;

create table if not exists request_type
(
	type varchar(255) not null
		constraint request_type_pkey
			primary key
)
;

alter table request_type owner to panagiotis;

create table if not exists sanitation_code_violations
(
	code_violation varchar(255) not null
		constraint sanitation_code_violations_pkey
			primary key
);

alter table sanitation_code_violations owner to panagiotis;

create table if not exists street_names
(
	street_name varchar(255) not null
		constraint street_names_pkey
			primary key
);

alter table street_names owner to panagiotis;

create table if not exists structures_with_graffiti
(
	structure varchar(255) not null
		constraint structures_with_graffiti_pkey
			primary key
);

alter table structures_with_graffiti owner to panagiotis;

create table if not exists surfaces_with_graffiti
(
	surface varchar(255) not null
		constraint surfaces_with_graffiti_pkey
			primary key
)
;

alter table surfaces_with_graffiti owner to panagiotis
;

create table if not exists vehicle_color
(
	color varchar(255) not null
		constraint vehicle_color_pkey
			primary key
)
;

alter table vehicle_color owner to panagiotis
;

create table if not exists vehicle_make_model
(
	make_model varchar(255) not null
		constraint vehicle_make_model_pkey
			primary key
)
;

alter table vehicle_make_model owner to panagiotis
;

create table if not exists zip_codes
(
	zip_code integer not null
		constraint zip_codes_pkey
			primary key
)
;

alter table zip_codes owner to panagiotis
;

create table if not exists actions
(
	action varchar(255) not null
		constraint actions_pkey
			primary key
)
;

alter table actions owner to panagiotis
;

create table if not exists activities
(
	activity varchar(255) not null
		constraint activities_pkey
			primary key
)
;

alter table activities owner to panagiotis
;

create table if not exists request
(
	id integer not null
		constraint request_pkey
			primary key,
	community_area integer,
	completion_date date,
	creation_date date,
	latitude double precision,
	longitude double precision,
	police_district integer,
	request_type varchar(255)
		constraint request_request_type_type_fk
			references request_type,
	service_request_number varchar(255),
	status varchar(255),
	street_name varchar(255)
		constraint request_street_names_street_name_fk
			references street_names,
	street_number varchar(255),
	ward integer,
	x_coordinate double precision,
	y_coordinate double precision,
	zip_code integer
		constraint request_zip_codes_zip_code_fk
			references zip_codes
)
;

alter table request owner to panagiotis
;

create table if not exists garbage_carts_delivered
(
	id integer not null
		constraint garbage_carts_delivered_pkey
			primary key
		constraint fkd1bdlo47e37udtt4lfi75gkee
			references request
		constraint garbage_carts_delivered_request_id_fk
			references request,
	number_of_carts double precision
)
;

alter table garbage_carts_delivered owner to panagiotis
;

create table if not exists graffiti_details
(
	id integer not null
		constraint graffiti_details_pkey
			primary key
		constraint fks3en62gg4t473tdm01fnuhtmh
			references request
		constraint graffiti_details_request_id_fk
			references request,
	structure_type varchar(255),
	surface_type varchar(255)
)
;

alter table graffiti_details owner to panagiotis
;

create table if not exists location_of_tree_in_request
(
	id integer not null
		constraint location_of_tree_in_request_pkey
			primary key
		constraint fk4jibqbawx9umtda1drjbpmb5l
			references request
		constraint location_of_tree_in_request_request_id_fk
			references request,
	tree_location varchar(255)
)
;

alter table location_of_tree_in_request owner to panagiotis
;

create table if not exists potholes_filled
(
	id integer not null
		constraint potholes_filled_pkey
			primary key
		constraint fkgn7t8ty6ps6k14jktth0u9nn4
			references request
		constraint potholes_filled_request_id_fk
			references request,
	number_of_filled_potholes double precision
)
;

alter table potholes_filled owner to panagiotis
;

create table if not exists recent_actions_on_request
(
	id integer not null
		constraint recent_actions_on_request_pkey
			primary key
		constraint fkontiyio9epw9m374fr88et833
			references request
		constraint recent_actions_on_request_request_id_fk
			references request,
	current_activity varchar(255),
	most_recent_action varchar(255)
)
;

alter table recent_actions_on_request owner to panagiotis
;

create table if not exists rodent_baiting_details
(
	id integer not null
		constraint rodent_baiting_details_pkey
			primary key
		constraint fki6gf0dja68571woorfnj0mexo
			references request
		constraint request_id
			references request,
	number_of_premises_baited integer,
	number_of_premises_with_garbage integer,
	number_of_premises_with_rats integer
)
;

alter table rodent_baiting_details owner to panagiotis
;

create table if not exists sanitation_violations_in_request
(
	id integer not null
		constraint sanitation_violations_in_request_pkey
			primary key
		constraint fkpv5s2gbkw6n47he4qwraew931
			references request
		constraint sanitation_violations_in_request_request_id_fk
			references request,
	violation varchar(255)
)
;

alter table sanitation_violations_in_request owner to panagiotis
;

create table if not exists ssa_in_request
(
	id integer not null
		constraint ssa_in_request_pkey
			primary key
		constraint fk25ufp43mqlb1pqctrn6yawwec
			references request
		constraint ssa_in_request_request_id_fk
			references request,
	ssa integer
)
;

alter table ssa_in_request owner to panagiotis
;

create table if not exists abandoned_vehicle_details
(
	id integer not null
		constraint abandoned_vehicle_details_pkey
			primary key
		constraint abandoned_vehicle_details_request_id_fk
			references request
		constraint fk2otq06hebv08659icp4560nxs
			references request
		constraint fkshh26xnk2sy84ucc8xde81ss9
			references request,
	license_plate varchar(255),
	number_of_dats_parked double precision,
	vehicle_color varchar(255),
	vehicle_make_model varchar(255)
)
;

alter table abandoned_vehicle_details owner to panagiotis
;

create table if not exists "user"
(
	username text not null
		constraint user_pkey
			primary key,
	firstname text,
	lastname text,
	password text,
	street text,
	streetno text,
	email varchar(255)
)
;

alter table "user" owner to panagiotis
;

create table if not exists logs
(
	id serial not null
		constraint logs_pkey
			primary key,
	event varchar(1024),
	timestamp timestamp,
	username varchar(255)
)
;

alter table logs owner to panagiotis
;

create table if not exists user_logs
(
	username text not null
		constraint fkfxagaggmoxfpbpp4cgwtseft4
			references "user",
	id integer not null
		constraint fkt5rowtswc4c260rigjsau3dlc
			references logs,
	constraint user_role_pkey
		primary key (id, username)
)
;

alter table user_logs owner to panagiotis
;

create table if not exists request_history
(
	id integer,
	community_area integer,
	completion_date date,
	creation_date date,
	latitude double precision,
	longitude double precision,
	police_district integer,
	request_type varchar(255),
	service_request_number varchar(255),
	status varchar(255),
	street_name varchar(255),
	street_number varchar(255),
	ward integer,
	x_coordinate double precision,
	y_coordinate double precision,
	zip_code integer,
	changed_on timestamp
)
;

alter table request_history owner to panagiotis
;

CREATE INDEX request_type_idx ON request(request_type);
CREATE INDEX zip_code_idx ON request(zip_code);
CREATE INDEX police_dists_idx ON request(police_district);
CREATE INDEX vehicle_color_idx ON abandoned_vehicle_details(vehicle_color);
