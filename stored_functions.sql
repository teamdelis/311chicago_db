-- STORED FUNCTION 1 
-- total requests per type, within a specified time range, in a descending order
CREATE OR REPLACE FUNCTION total_requests_per_type_in_period(from_date DATE, to_date DATE, includeDuplicates BOOLEAN)
RETURNS TABLE (type_of_request VARCHAR(45), request_count bigint)
AS $$
BEGIN

IF includeDuplicates THEN
	RETURN QUERY SELECT request_type, count(*) AS counted_requests
				 FROM request
				 WHERE creation_date >= from_date AND creation_date <= to_date
				 GROUP BY request_type
				 ORDER BY counted_requests DESC;
ELSE
	RETURN QUERY SELECT request_type, count(*) AS counted_requests
				 FROM request
				 WHERE creation_date >= from_date AND creation_date <= to_date AND
			 		   status <> 'Open - Dup' AND status <> 'Completed - Dup'
				 GROUP BY request_type
				 ORDER BY counted_requests DESC;
END IF;

END; $$
LANGUAGE 'plpgsql';

-- STORED FUNCTION 2
-- total requests for each day for a specific request type and time range
CREATE OR REPLACE FUNCTION total_requests_per_day_for_type_in_period(from_date DATE, to_date DATE, type_of_request VARCHAR(45), includeDuplicates BOOLEAN)
RETURNS TABLE (day_of_creation DATE, request_count bigint)
AS $$
BEGIN

IF includeDuplicates THEN
	RETURN QUERY SELECT creation_date, count(*) AS counted_requests
				 FROM request
				 WHERE creation_date >= from_date AND creation_date <= to_date
				 	AND request_type = type_of_request
				 GROUP BY creation_date
				 ORDER BY creation_date;
ELSE
	RETURN QUERY SELECT creation_date, count(*) AS counted_requests
				 FROM request
				 WHERE creation_date >= from_date AND creation_date <= to_date
				 	AND request_type = type_of_request
				 	AND status <> 'Open - Dup' AND status <> 'Completed - Dup'
				 GROUP BY creation_date
				 ORDER BY creation_date DESC;
END IF;

END; $$
LANGUAGE 'plpgsql';

-- avg requests per day for a specific request type and time range
CREATE OR REPLACE FUNCTION avg_requests_per_day_for_type_in_period(from_date DATE, to_date DATE, type_of_request VARCHAR(45), includeDuplicates BOOLEAN)
RETURNS TABLE (requests_per_day NUMERIC(5,2))
AS $$
DECLARE number_of_days INT;
BEGIN

number_of_days := (to_date-from_date);
IF includeDuplicates THEN
	RETURN QUERY SELECT CAST( CAST(COUNT(*) as FLOAT)/number_of_days as NUMERIC(5,2)) AS requests_per_day
				 FROM request
				 WHERE creation_date >= from_date AND creation_date <= to_date
				 	AND request_type = type_of_request;
ELSE
	RETURN QUERY SELECT CAST( CAST(COUNT(*) as FLOAT)/number_of_days as NUMERIC(5,2)) AS requests_per_day
				 FROM request
				 WHERE creation_date >= from_date AND creation_date <= to_date
				 	AND request_type = type_of_request
				 	AND status <> 'Open - Dup' AND status <> 'Completed - Dup';
END IF;

END; $$
LANGUAGE 'plpgsql';

-- STORED FUNCTION 3
-- most common service request per zipcode for a specific day
CREATE OR REPLACE FUNCTION most_common_request_type_per_zipcode_in_day(day DATE, includeDuplicates BOOLEAN)
RETURNS TABLE (zipcode int, type_of_request VARCHAR(45), counted_requests BIGINT)
AS $$
BEGIN

IF includeDuplicates THEN
	RETURN QUERY SELECT DISTINCT ON (zip_code) zip_code, request_type, count(*) AS counted_type
				 FROM request
				 WHERE creation_date = day AND zip_code IS NOT NULL
				 GROUP BY zip_code, request_type
				 ORDER BY zip_code, counted_type DESC;

ELSE
	RETURN QUERY SELECT DISTINCT ON (zip_code) zip_code, request_type, count(*) AS counted_type
				 FROM request
				 WHERE creation_date = day AND zip_code IS NOT NULL
				 	AND status <> 'Open - Dup' AND status <> 'Completed - Dup'
				 GROUP BY zip_code, request_type
				 ORDER BY zip_code, counted_type DESC;

END IF;

END; $$
LANGUAGE 'plpgsql';

-- STORED FUNCTION 4
-- average completion time per service request for a specific date range
-- per request
CREATE OR REPLACE FUNCTION avg_request_completion_time_in_period(from_date DATE, to_date DATE, includeDuplicates BOOLEAN)
RETURNS TABLE (avg_completion_time NUMERIC(5,2))
AS $$
BEGIN

IF includeDuplicates THEN
	RETURN QUERY SELECT CAST(AVG(completion_date-creation_date) AS NUMERIC(5,2))
				 FROM request
				 WHERE completion_date IS NOT NULL
				 	AND creation_date >= from_date
				 	AND creation_date <= to_date;
ELSE
	RETURN QUERY SELECT CAST(AVG(completion_date-creation_date) AS NUMERIC(5,2))
				 FROM request
				 WHERE status <> 'Completed - Dup'
				 	AND completion_date IS NOT NULL
				 	AND creation_date >= from_date
				 	AND creation_date <= to_date;
END IF;

END; $$
LANGUAGE 'plpgsql';

-- per request type
CREATE OR REPLACE FUNCTION avg_request_completion_time_per_type_in_period(from_date DATE, to_date DATE, includeDuplicates BOOLEAN)
RETURNS TABLE (type_of_request VARCHAR(45), avg_completion_time NUMERIC(5,2))
AS $$
BEGIN

IF includeDuplicates THEN
	RETURN QUERY SELECT request_type, CAST(AVG(completion_date-creation_date) AS NUMERIC(5,2)) AS completion_time
				 FROM request
				 WHERE completion_date IS NOT NULL
				 	AND creation_date >= from_date
				 	AND creation_date <= to_date
			 	 GROUP BY request_type
			 	 ORDER BY completion_time;
ELSE
	RETURN QUERY SELECT request_type, CAST(AVG(completion_date-creation_date) AS NUMERIC(5,2)) AS completion_time
				 FROM request
				 WHERE status <> 'Completed - Dup'
				 	AND completion_date IS NOT NULL
				 	AND creation_date >= from_date
				 	AND creation_date <= to_date
			 	 GROUP BY request_type
			 	 ORDER BY completion_time;
END IF;

END; $$
LANGUAGE 'plpgsql';


-- STORED FUNCTION 5
-- most common service request in a specified bounding box (GPS-coordinates) for a specific day
CREATE OR REPLACE FUNCTION most_common_request_type_within_coords(x1 FLOAT, y1 FLOAT, x2 FLOAT, y2 FLOAT, day DATE, includeDuplicates BOOLEAN)
RETURNS TABLE (type_of_request VARCHAR(45), counted_requests BIGINT)
AS $$
BEGIN

IF includeDuplicates THEN
	RETURN QUERY SELECT request_type, count(*) AS counted
				 FROM request
				 WHERE creation_date = day
				 	AND latitude IS NOT NULL AND longitude IS NOT NULL
				 	AND BOX(POINT(x1,y1),POINT(x2,y2)) @> POINT(latitude, longitude)
				 GROUP BY request_type
		 		 ORDER BY counted DESC
				 LIMIT 1;
ELSE
	RETURN QUERY SELECT request_type, count(*) AS counted
				 FROM request
				 WHERE status <> 'Completed - Dup'
				 	AND status <> 'Open - Dup'
				 	AND creation_date = day
				 	AND latitude IS NOT NULL AND longitude IS NOT NULL
				 	AND BOX(POINT(x1,y1),POINT(x2,y2)) @> POINT(latitude, longitude)
				 GROUP BY request_type
		 		 ORDER BY counted DESC
				 LIMIT 1;
END IF;

END; $$
LANGUAGE 'plpgsql';


-- STORED FUNCTION 6
-- top-5 (SSA) in total number of requests per day for a specific date range
CREATE OR REPLACE FUNCTION top5_ssa_in_requests_per_day_in_period(from_date DATE, to_date DATE, includeDuplicates BOOLEAN)
RETURNS TABLE (ssa INT, counted_requests NUMERIC(5,2))
AS $$
BEGIN

IF includeDuplicates THEN
	RETURN QUERY SELECT a.ssa, CAST( CAST(COUNT(*) as FLOAT)/(to_date-from_date) as NUMERIC(5,2)) AS requests_per_day 
				 FROM ssa_in_request a, request b
				 WHERE a.request_id = b.id
				 	AND b.creation_date >= from_date
				 	AND b.creation_date <= to_date
				 GROUP BY a.ssa
				 ORDER BY requests_per_day DESC
				 LIMIT 5;
ELSE
	RETURN QUERY SELECT a.ssa, CAST( CAST(COUNT(*) as FLOAT)/(to_date-from_date) as NUMERIC(5,2)) AS requests_per_day 
				 FROM ssa_in_request a, request b
				 WHERE a.request_id = b.id
				 	AND b.creation_date >= from_date
				 	AND b.creation_date <= to_date
				 	AND status <> 'Completed - Dup'
				 	AND status <> 'Open - Dup'
				 GROUP BY a.ssa
				 ORDER BY requests_per_day DESC
				 LIMIT 5;
END IF;

END; $$
LANGUAGE 'plpgsql';


-- STORED FUNCTION 7
-- license plates (if any) in abandoned vehicle complaints more than once
CREATE OR REPLACE FUNCTION license_plate_involved_in_multiple_requests(includeDuplicates BOOLEAN)
RETURNS TABLE (license_plate_no VARCHAR(351), counted_requests BIGINT)
AS $$
BEGIN

IF includeDuplicates THEN
    RETURN QUERY SELECT license_plate, count(*) AS request_count
                 FROM abandoned_vehicle_details
                 WHERE license_plate IS NOT NULL
                 GROUP BY license_plate
                 HAVING count(*) > 1
                 ORDER BY request_count DESC;
ELSE
    RETURN QUERY SELECT a.license_plate, count(*) AS request_count
                 FROM abandoned_vehicle_details a, request r
                 WHERE a.request_id = r.id AND a.license_plate IS NOT NULL
                    AND r.status <> 'Open - Dup' AND r.status <> 'Completed - Dup'
                 GROUP BY a.license_plate
                 HAVING  count(*) > 1
                 ORDER BY request_count DESC;
END IF;

END; $$
LANGUAGE 'plpgsql';


-- STORED FUNCTION 8
-- the second most common color of vehicles involved in abandoned vehicle complaints
CREATE OR REPLACE FUNCTION second_most_common_color_of_abandoned_vehicle(includeDuplicates BOOLEAN)
RETURNS TABLE (second_most_popular_color VARCHAR(15), counted_requests BIGINT)
AS $$
BEGIN

IF includeDuplicates THEN
    RETURN QUERY SELECT vehicle_color, count(*) AS request_count
                 FROM abandoned_vehicle_details
                 WHERE vehicle_color IS NOT NULL
                 GROUP BY vehicle_color
                 ORDER BY request_count DESC
                 LIMIT 1 OFFSET 1;
ELSE
    RETURN QUERY SELECT a.vehicle_color, count(*) AS request_count
                 FROM abandoned_vehicle_details a, request r
                 WHERE a.request_id = r.id AND a.vehicle_color IS NOT NULL
                 AND r.status <> 'Open - Dup' AND r.status <> 'Completed - Dup'
                 GROUP BY a.vehicle_color
                 ORDER BY request_count DESC
                 LIMIT 1 OFFSET 1;
END IF;

END; $$
LANGUAGE 'plpgsql';


-- STORED FUNCTION 9
-- the rodent baiting requests with number of premises baited less than the specified
CREATE OR REPLACE FUNCTION requests_with_premises_baited_less_than_specified(number_of_premises INT, includeDuplicates BOOLEAN)
RETURNS TABLE (request_number VARCHAR(15), created_on DATE, completed_on DATE, street VARCHAR(45), street_no VARCHAR(10), no_of_premises INT)
AS $$
BEGIN

IF includeDuplicates THEN
    RETURN QUERY SELECT a.service_request_number, a.creation_date, a.completion_date, a.street_name, a.street_number, b.number_of_premises_baited
                 FROM request a, rodent_baiting_details b
                 WHERE a.id = b.request_id AND b.number_of_premises_baited IS NOT NULL
                 	AND b.number_of_premises_baited > 0
                 	AND b.number_of_premises_baited < number_of_premises;
ELSE
    RETURN QUERY SELECT a.service_request_number, a.creation_date, a.completion_date, a.street_name, a.street_number, b.number_of_premises_baited
                 FROM request a, rodent_baiting_details b
                 WHERE a.id = b.request_id AND b.number_of_premises_baited IS NOT NULL
                 	AND b.number_of_premises_baited > 0
                 	AND b.number_of_premises_baited < number_of_premises
                    AND a.status <> 'Open - Dup' AND a.status <> 'Completed - Dup';
END IF;

END; $$
LANGUAGE 'plpgsql';

-- STORED FUNCTION 10
-- the rodent baiting requests with number of premises with garbage less than the specified
CREATE OR REPLACE FUNCTION requests_with_premises_with_garbage_less_than_specified(number_of_premises INT, includeDuplicates BOOLEAN)
RETURNS TABLE (request_number VARCHAR(15), created_on DATE, completed_on DATE, street VARCHAR(45), street_no VARCHAR(10), no_of_premises INT)
AS $$
BEGIN

IF includeDuplicates THEN
    RETURN QUERY SELECT a.service_request_number, a.creation_date, a.completion_date, a.street_name, a.street_number, b.number_of_premises_with_garbage
                 FROM request a, rodent_baiting_details b
                 WHERE a.id = b.request_id AND b.number_of_premises_with_garbage IS NOT NULL
                 	AND b.number_of_premises_with_garbage > 0
                 	AND b.number_of_premises_with_garbage < number_of_premises;
ELSE
    RETURN QUERY SELECT a.service_request_number, a.creation_date, a.completion_date, a.street_name, a.street_number, b.number_of_premises_with_garbage
                 FROM request a, rodent_baiting_details b
                 WHERE a.id = b.request_id AND b.number_of_premises_with_garbage IS NOT NULL
                 	AND b.number_of_premises_with_garbage > 0
                 	AND b.number_of_premises_with_garbage < number_of_premises
                    AND a.status <> 'Open - Dup' AND a.status <> 'Completed - Dup';
END IF;

END; $$
LANGUAGE 'plpgsql';

-- STORED FUNCTION 11
-- the rodent baiting requests with number of premises with rats less than the specified
CREATE OR REPLACE FUNCTION requests_with_premises_with_rats_less_than_specified(number_of_premises INT, includeDuplicates BOOLEAN)
RETURNS TABLE (request_number VARCHAR(15), created_on DATE, completed_on DATE, street VARCHAR(45), street_no VARCHAR(10), no_of_premises INT)
AS $$
BEGIN

IF includeDuplicates THEN
    RETURN QUERY SELECT a.service_request_number, a.creation_date, a.completion_date, a.street_name, a.street_number, b.number_of_premises_with_rats
                 FROM request a, rodent_baiting_details b
                 WHERE a.id = b.request_id AND b.number_of_premises_with_rats IS NOT NULL
                 	AND b.number_of_premises_with_rats > 0
                 	AND b.number_of_premises_with_rats < number_of_premises;
ELSE
    RETURN QUERY SELECT a.service_request_number, a.creation_date, a.completion_date, a.street_name, a.street_number, b.number_of_premises_with_rats
                 FROM request a, rodent_baiting_details b
                 WHERE a.id = b.request_id AND b.number_of_premises_with_rats IS NOT NULL
                 	AND b.number_of_premises_with_rats > 0
                 	AND b.number_of_premises_with_rats < number_of_premises
                    AND a.status <> 'Open - Dup' AND a.status <> 'Completed - Dup';
END IF;

END; $$
LANGUAGE 'plpgsql';

-- STORED FUNCTION 12
-- the police districts that have handled “pot holes” requests with more than one number
-- of potholes on the same day that they also handled “rodent baiting” requests with more than
-- one number of premises baited, for a specific day.
CREATE OR REPLACE FUNCTION police_districts_handling_potholes_and_rodent_baiting_in_day(day DATE)
RETURNS TABLE (p_district INT)
AS $$
BEGIN

    RETURN QUERY SELECT a.police_district
                 FROM request a, potholes_filled b
                 WHERE a.id = b.request_id
                 	AND a.police_district IS NOT NULL
                    AND b.number_of_filled_potholes IS NOT NULL
                    AND b.number_of_filled_potholes > 1
                    AND a.completion_date = day
                 INTERSECT
                 SELECT a.police_district
                 FROM request a, rodent_baiting_details b
                 WHERE a.id = b.request_id
                 	AND a.police_district IS NOT NULL
                    AND b.number_of_premises_baited IS NOT NULL
                    AND b.number_of_premises_baited > 1
                    AND a.completion_date = day;

END; $$
LANGUAGE 'plpgsql';
